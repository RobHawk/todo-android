package robhawk.com.br.todo.data.repository;

import io.reactivex.Maybe;
import robhawk.com.br.todo.data.db.TodoAppDatabase;
import robhawk.com.br.todo.data.db.dao.UserDao;
import robhawk.com.br.todo.data.model.User;

public class UserRepository {

    private final UserDao mDao;

    public UserRepository() {
        mDao = TodoAppDatabase.getInstance().userDao();
    }

    public Maybe<User> findByEmailPassword(final String email, final String password) {
        return mDao.findByEmailPassword(email, password);
    }

    public Maybe<Boolean> insert(final User user) {
        return mDao.findByEmail(user.email)
                .map(userWithSameEmail -> {
                    if (userWithSameEmail == null)
                        user.id = (int) mDao.insert(user);
                    return user.id > 0;
                });
    }

}
