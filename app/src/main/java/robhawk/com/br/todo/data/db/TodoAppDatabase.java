package robhawk.com.br.todo.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import robhawk.com.br.todo.data.db.dao.TaskDao;
import robhawk.com.br.todo.data.db.dao.UserDao;
import robhawk.com.br.todo.data.model.Task;
import robhawk.com.br.todo.data.model.User;

@Database(entities = {User.class, Task.class}, version = 1)
public abstract class TodoAppDatabase extends RoomDatabase {

    private static TodoAppDatabase INSTANCE;

    public static void init(@NonNull final Context context) {
        if (INSTANCE != null) return;
        INSTANCE = Room.databaseBuilder(context, TodoAppDatabase.class, "todo")
                .fallbackToDestructiveMigration()
                .build();
    }

    public static TodoAppDatabase getInstance() {
        if (INSTANCE == null)
            throw new ExceptionInInitializerError("Call init() once before getInstance()");
        return INSTANCE;
    }

    public abstract UserDao userDao();

    public abstract TaskDao taskDao();

}
