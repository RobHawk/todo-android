package robhawk.com.br.todo.ui.task;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import robhawk.com.br.todo.R;
import robhawk.com.br.todo.data.model.User;

public class TaskActivity extends AppCompatActivity {

    private static final String USER_INTENT = "user-intent";

    public static Intent newIntent(Context context, User user) {
        Intent intent = new Intent(context, TaskActivity.class);
        intent.putExtra(USER_INTENT, user);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
    }

    public User getUserIntent() {
        return (User) getIntent().getSerializableExtra(USER_INTENT);
    }
}
