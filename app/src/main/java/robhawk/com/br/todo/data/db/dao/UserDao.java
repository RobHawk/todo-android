package robhawk.com.br.todo.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import robhawk.com.br.todo.data.model.User;

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(User user);

    @Query("SELECT * FROM user WHERE email = :email")
    Maybe<User> findByEmail(String email);

    @Query("SELECT * FROM user WHERE email = :email AND password = :password")
    Maybe<User> findByEmailPassword(String email, String password);

    @Query("SELECT * FROM user")
    Flowable<List<User>> listAll();

}
