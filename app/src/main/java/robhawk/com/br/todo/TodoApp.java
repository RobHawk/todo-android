package robhawk.com.br.todo;

import android.app.Application;

import robhawk.com.br.todo.data.db.TodoAppDatabase;

public class TodoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TodoAppDatabase.init(this);
    }
}
