package robhawk.com.br.todo.ui.user;

import robhawk.com.br.todo.data.model.User;

public interface UserNavigator {

    void openRegister();

    void openLogin();

    void loginSuccess(User auth);

    void showRequiredFieldsEmpty();

    void showPasswordsDoNotMatch();

    void showEmailAlreadyExists(String email);

}
