package robhawk.com.br.todo.ui.user;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

import robhawk.com.br.todo.data.model.User;
import robhawk.com.br.todo.data.repository.UserRepository;

import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

public class UserViewModel extends ViewModel {

    private final ObservableField<User> mUserObservable;
    public final ObservableField<String> passwordConfirmation;
    private final UserRepository mRepository;
    private UserNavigator mNavigator;

    public UserViewModel() {
        mUserObservable = new ObservableField<>();
        mUserObservable.set(new User());
        passwordConfirmation = new ObservableField<>();
        mRepository = new UserRepository();
    }

    public User getUser() {
        return mUserObservable.get();
    }

    public void setNavigator(UserNavigator navigator) {
        mNavigator = navigator;
    }

    public void login() {
        User user = getUser();
        mRepository.findByEmailPassword(user.email, user.password)
                .subscribeOn(io())
                .observeOn(mainThread())
                .doOnSuccess(auth -> mNavigator.loginSuccess(auth))
                .subscribe();
    }

    public void openRegister() {
        mNavigator.openRegister();
    }

    public void register() {
        final User user = getUser();

        if (isInvalid(user))
            return;

        mRepository.insert(user)
                .subscribeOn(io())
                .observeOn(mainThread())
                .doOnSuccess(registeredEmail -> {
                    if (registeredEmail)
                        mNavigator.openLogin();
                    else
                        mNavigator.showEmailAlreadyExists(user.email);
                })
                .subscribe();
    }

    private boolean isInvalid(User user) {
        return isAnyRequiredFieldEmpty(user) || isPasswordsDoNotMatch(user.password);
    }

    private boolean isPasswordsDoNotMatch(String password) {
        boolean invalid = !password.equals(passwordConfirmation.get());
        if (invalid)
            mNavigator.showPasswordsDoNotMatch();
        return invalid;
    }

    private boolean isAnyRequiredFieldEmpty(User user) {
        boolean invalid = user.name.isEmpty() || user.password.isEmpty() || user.email.isEmpty();
        if (invalid)
            mNavigator.showRequiredFieldsEmpty();
        return invalid;
    }

}
