package robhawk.com.br.todo.ui.user;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import robhawk.com.br.todo.R;
import robhawk.com.br.todo.data.model.User;
import robhawk.com.br.todo.databinding.ActivityUserBinding;
import robhawk.com.br.todo.ui.task.TaskActivity;

public class UserActivity extends AppCompatActivity implements UserNavigator {

    private ActivityUserBinding mBinding;
    private LoginFragment mLoginFragment;
    private RegisterFragment mRegisterFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UserViewModel viewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        viewModel.setNavigator(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user);
        mBinding.setVm(viewModel);

        mLoginFragment = new LoginFragment();
        mRegisterFragment = new RegisterFragment();

        openLogin();
    }

    @Override
    public void openLogin() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(mBinding.fragment.getId(), mLoginFragment)
                .commit();
    }

    @Override
    public void loginSuccess(User auth) {
        Intent taskActivity = TaskActivity.newIntent(this, auth);
        startActivity(taskActivity);
        finish();
    }

    @Override
    public void openRegister() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(mBinding.fragment.getId(), mRegisterFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void showRequiredFieldsEmpty() {
        String msg = getString(R.string.there_are_some_required_input_fields);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showPasswordsDoNotMatch() {
        String msg = getString(R.string.passwords_dont_match);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmailAlreadyExists(String email) {
        String msg = getString(R.string.email_x_is_already_registered);
        Toast.makeText(this, String.format(msg, email), Toast.LENGTH_SHORT).show();
    }

}
