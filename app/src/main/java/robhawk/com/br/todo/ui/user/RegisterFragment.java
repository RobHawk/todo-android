package robhawk.com.br.todo.ui.user;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import robhawk.com.br.todo.databinding.FragmentRegisterBinding;

public class RegisterFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        UserViewModel viewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);
        FragmentRegisterBinding binding = FragmentRegisterBinding.inflate(inflater, container, false);
        binding.setVm(viewModel);
        return binding.getRoot();
    }

}
